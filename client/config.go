// COPYRIGHT

package client

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/liquidgecka/logsaver/client/notify"
)

type Config struct {
	HTTPClient              *http.Client
	Backoff                 time.Duration
	MaximumBytesPerRequest  int64
	MaximumRetries          int
	ProcessingTimeout       time.Duration
	RequestTimeout          time.Duration
	ManualFileCheckInterval time.Duration
	ParentContext           context.Context
	Notify                  notify.Notify
}
