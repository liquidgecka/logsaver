// COPYRIGHT

package client

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/liquidgecka/logsaver/client/notify"
)

// The configuration object that gets passed into the NewDirWatcher function.
// This lets the configuration get populated some place other than a 10
// argument long function call.
type DirWatcherConfig struct {
	// REQUIRED: the directory to watch.
	Directory string

	// REQUIRED: A glob of files that should be included when scanning the
	// directory. If a file matches an include glob, and does not match any
	// exclude globs then it will be uploaded, otherwise it will be
	// ignored.
	IncludeGlobs []string
	ExcludeGlobs []string

	// REQUIRED: The URL that files will be uploaded under. If this is
	// http://example.com/x/y/z then all the files will be uploaded to
	// http://example.com/x/y/z/<filename>.
	URL string

	// REQUIRED: The name of the File implementation that will be used. This
	// must be a valid type for use with NewFile().
	FileType string

	// If this is nil then the default HTTP client in the net.http package
	// will be used.
	HTTPClient *http.Client

	// The amount of time that the processor will sleep after failing a HTTP
	// request. This ensures that the process doesn't busy loop a failing
	// request.
	Backoff time.Duration

	// The files are uploaded in chunks to the remote Logsaver server. This
	// is the maximum size of each chunk. Setting this too high increases
	// overhead of failed calls, and setting this too low increases bandwidth
	// consumption.
	MaximumBytesPerRequest int64

	// The maximum number of times that a HTTP request will be retried before
	// the file is considered failed.
	MaximumRetries int

	// The maximum amount of time that a request will be allowed to be
	// attempted before its considered a total failure.
	ProcessingTimeout time.Duration

	// The amount of time to give a single HTTP request before failing it. Note
	// that there must be enough time for the file chunk specified earlier to
	// upload or this will timeout all requests.
	RequestTimeout time.Duration

	// The directory being watched will be scanned this frequently in order
	// to look for files that the notification system didn't alert on. This
	// is just a safety check to ensure that nothing gets lost and should not
	// be set too low.
	ManualScanInterval time.Duration

	// If a file has not been notified that it has updated in this period of
	// time then it will automatically perform a os.Stat() call to see if the
	// file has changed. This ensures that we do not lose file updates
	// completely.
	ManualFileCheckInterval time.Duration

	// If defined then the context for the DirWatcher will be a child of the
	// given Context so they can all be canceled together.
	ParentContext context.Context

	// The notification library that should be used for watching the directory
	// for updates.
	Notify notify.Notify
}

// Creates a new DirWatcher from the given Config.
func NewDirWatcher(c *DirWatcherConfig) (DirWatcher, error) {
	d := &dirWatcher{
		directory:              c.Directory,
		includeGlobs:           c.IncludeGlobs,
		excludeGlobs:           c.ExcludeGlobs,
		baseURL:                c.URL,
		fileType:               c.FileType,
		httpClient:             c.HTTPClient,
		backoff:                c.Backoff,
		manualScanInterval:     c.ManualScanInterval,
		maximumBytesPerRequest: c.MaximumBytesPerRequest,
		maximumRetries:         c.MaximumRetries,
		requestTimeout:         c.RequestTimeout,
		processingTimeout:      c.ProcessingTimeout,
		shutdown:               0,
		shutdownChan:           make(chan struct{}),
		files:                  make(map[string]File, 1000),
		filesUpdated:           make(chan struct{}, 1),
		notify:                 c.Notify,
	}

	// Sanity check the inputs.
	if d.directory == "" {
		return nil, fmt.Errorf("Directory not defined.")
	} else if s, err := os.Stat(d.directory); err != nil {
		return nil, err
	} else if !s.IsDir() {
		return nil, fmt.Errorf("%s is not a directory.", d.directory)
	}
	if len(d.includeGlobs) == 0 {
		d.includeGlobs = []string{"*"}
	}
	if d.baseURL == "" {
		return nil, fmt.Errorf("URL can not be empty.")
	} else if u, err := url.Parse(d.baseURL); err != nil {
		return nil, err
	} else if u.Scheme != "http" && u.Scheme != "https" {
		return nil, fmt.Errorf("URL scheme is not HTTP(S?): %s\n", u.Scheme)
	} else if u.Host == "" {
		return nil, fmt.Errorf("URL scheme is missing a hostname.")
	}
	if d.httpClient == nil {
		d.httpClient = http.DefaultClient
	}
	if d.backoff == 0 {
		d.backoff = time.Second
	} else if d.backoff < 0 {
		return nil, fmt.Errorf("Backoff is negative!")
	}
	if d.manualScanInterval == 0 {
		d.manualScanInterval = time.Minute
	} else if d.manualScanInterval < 0 {
		return nil, fmt.Errorf("ManualScanInterval is negative!")
	}
	if d.manualFileCheckInterval == 0 {
		d.manualFileCheckInterval = time.Minute
	} else if d.manualFileCheckInterval < 0 {
		return nil, fmt.Errorf("ManualFileCheckInterval is negative!")
	}
	if d.maximumBytesPerRequest < 1024 {
		return nil, fmt.Errorf("MaximumBytesPreRequest is less than 1kib.")
	}
	if d.maximumRetries < 1 {
		return nil, fmt.Errorf("MaximumRetries is less than 1.")
	}
	if d.requestTimeout == 0 {
		d.requestTimeout = time.Second * 30
	} else if d.requestTimeout < 0 {
		return nil, fmt.Errorf("RequestTimeout is less than 0!")
	}
	if d.processingTimeout == 0 {
		d.processingTimeout = time.Minute * 5
	} else if d.processingTimeout < 0 {
		return nil, fmt.Errorf("ProcessingTimeout is less than 0!")
	}
	if d.notify == nil {
		var err error
		d.notify, err = notify.NewNotify()
		if err != nil {
			return nil, err
		}
		go d.notify.Run()
	}
	if c.ParentContext == nil {
		d.context, d.cancel = context.WithCancel(context.Background())
	} else {
		d.context, d.cancel = context.WithCancel(c.ParentContext)
	}

	return d, nil
}

type DirWatcher interface {
	// Starts the watcher.
	Run() error

	// Shut down the watcher, waiting on all existing files to be uploaded
	// but not starting new ones.
	Shutdown()

	// Force stops the watcher without completing any existing operations.
	Stop()
}

// The private implementation of DirWatcher that actually implements the
// logic of the directory watching.
type dirWatcher struct {
	// The directory that will be watched for new files and the
	// inclusion/exclusion globs that will be used to match files.
	directory    string
	includeGlobs []string
	excludeGlobs []string

	// The URL that will be used as the base for the destination. Each file
	// that gets uploaded will have a unique URL destination generated based
	// on several properties of the file.
	baseURL string

	// Configuration settings copied from the configuration object.
	fileType                string
	backoff                 time.Duration
	httpClient              *http.Client
	manualScanInterval      time.Duration
	manualFileCheckInterval time.Duration
	maximumBytesPerRequest  int64
	maximumRetries          int
	requestTimeout          time.Duration
	processingTimeout       time.Duration

	// The function that is used to generate new File() objects. There can
	// be many different types of File implementations.
	newFile func(name string, c *Config) File

	// A context that tracks processing of this configuration and a int
	// that we can use to mark when Shutdown() has been called.
	context      context.Context
	cancel       func()
	shutdown     int32
	shutdownChan chan struct{}

	// A map of file names to individual file processors. This is used to
	// evaluate if we already have a processor working on a given file or
	// if a new one needs created.
	files        map[string]File
	filesLock    sync.Mutex
	filesUpdated chan struct{}

	// An implementation of a file system notifier that will call back to
	// this watcher when a file in the directory that it is watching has
	// changed, been created, or been deleted. Typically the notifier is
	// shared between all of the Watchers running in the same
	// process.
	notify notify.Notify

	// A wait group that is used for tracking child goroutines that are
	// started within the Run() call.
	workersWG sync.WaitGroup
}

// Runs the DirWatcher process. This will watch the directory for changes,
// and if a file changes or or created then it will start a processor. This
// will return an error if there is a problem scanning the directory in
// some way.
func (d *dirWatcher) Run() error {
	// Start off by setting up a watch on the directory via our Notify object.
	if err := d.notify.WatchDirectory(d.directory, d); err != nil {
		return err
	}

	// Next we need to ensure that the directory gets scanned manually
	// initially so that all existing logs get found and processing gets
	// started on them.
	nextScan := time.Time{}

	// The notifier will trigger file update notifications via a callback,
	// so this routine will simply watch the directory manually to ensure
	// that not updates get missed or lost in the process.
	for d.shutdown == 0 {
		// Sleep until the next manual scan is required.
		sleepTime := time.Until(nextScan)
		if sleepTime > 0 {
			timer := time.NewTimer(sleepTime)
			select {
			case <-timer.C:
				// Sleep complete, time to perform the manual scan.
			case <-d.shutdownChan:
				// Return the loop to the for check so that it can kick out
				// and initiate the graceful shutdown cycle.
				break
			}
		}

		// Make sure that the next scan happens after a period of time
		// configured in the manualScanInterval value.
		nextScan = time.Now().Add(d.manualScanInterval)

		// Manually scan the directory. Each file gets processed and then
		// if its something we want to scan it gets tracked.
		files, err := ioutil.ReadDir(d.directory)
		if err != nil {
			return fmt.Errorf(
				"Error listing the files in '%s': %s'",
				d.directory,
				err.Error())
		}
		for _, file := range files {
			d.considerFile(file.Name(), file)
		}

	}

	// Getting here means that the process has been shutdown or stopped. We
	// need to stop all of the file processes that we have knowledge of and
	// then wait on them to all finish.
	d.filesLock.Lock()
	for _, file := range d.files {
		file.Shutdown()
	}
	d.filesLock.Unlock()

	// Next we need to wait for either the files processes to shutdown
	// gracefully or for a Stop() to be called.
	for d.context.Err() != context.Canceled {
		// If there are no files left being processed then we can break out
		// of this loop.
		d.filesLock.Lock()
		l := len(d.files)
		d.filesLock.Unlock()
		if l == 0 {
			break
		}
		select {
		case <-d.context.Done():
			break
		case <-d.filesUpdated:
			continue
		}
	}

	// Getting here means that we have either been stopped() or have gracefully
	// shutdown and there are no more running files.
	d.filesLock.Lock()
	for _, file := range d.files {
		file.Stop()
	}
	d.filesLock.Unlock()

	// Success!
	return nil
}

// Performs a graceful shutdown. This will stop scanning for new files but
// will continue processing old files until they have all been successfully
// uploaded. This function will return right away, not when the main Run()
// loop has terminated.
func (d *dirWatcher) Shutdown() {
	atomic.StoreInt32(&d.shutdown, 1)
	close(d.shutdownChan)
}

// Stops the running process. This will cancel all running requests and will
// stop all uploads as quickly as possible. This is for a non graceful shutdown
// of services. This function will return immediately.
func (d *dirWatcher) Stop() {
	atomic.StoreInt32(&d.shutdown, 1)
	close(d.shutdownChan)
	d.cancel()
}

// Looks at the status of a file and see if it needs to added for processing
// by this watcher.
func (d *dirWatcher) considerFile(name string, fi os.FileInfo) {
	// The only possible error that Match returns if when the pattern
	// is bad, and as such we consider any error to be a non match.
	matched := false
	for _, glob := range d.includeGlobs {
		if match, err := filepath.Match(glob, name); err != nil {
			continue
		} else if match {
			matched = true
			break
		}
	}
	if !matched {
		return
	}
	for _, glob := range d.excludeGlobs {
		if match, err := filepath.Match(glob, name); err != nil {
			return
		} else if match {
			return
		}
	}

	// Ensure that the stat info exists.
	if fi == nil {
		var err error
		fi, err = os.Lstat(filepath.Join(d.directory, name))
		if err != nil {
			// FIXME!
			fmt.Printf("ERROR: %d: %s\n", name, err.Error())
			return
		}
	}

	// Getting this far means that the file is worth considering. Next we
	// need to figure out if the thing passed in is a file since we can not
	// process anything but files.
	if !fi.Mode().IsRegular() {
		// FIXME: log?
		return
	}

	// If the file already exists then we can notify the file processor
	// directly.
	d.filesLock.Lock()
	file, ok := d.files[name]
	if ok {
	} else {
		conf := &Config{
			HTTPClient:              d.httpClient,
			MaximumBytesPerRequest:  d.maximumBytesPerRequest,
			MaximumRetries:          d.maximumRetries,
			ProcessingTimeout:       d.processingTimeout,
			RequestTimeout:          d.requestTimeout,
			ManualFileCheckInterval: d.manualFileCheckInterval,
			ParentContext:           d.context,
			Notify:                  nil,
		}
		file = NewFile(
			d.fileType,
			d.directory,
			name,
			d.baseURL,
			conf)
		d.files[name] = file
		go d.runFile(name, file)
	}
	d.filesLock.Unlock()
	file.Updated()
}

// Runs a file and then removes it from the map when it exits.
func (d *dirWatcher) runFile(name string, f File) {
	if err := f.Run(); err != nil {
		fmt.Printf("Error processing %s: %s\n", name, err.Error())
	}
	f.Stop()
	d.filesLock.Lock()
	delete(d.files, name)
	d.filesLock.Unlock()
	d.filesUpdated <- struct{}{}
}

// Gets called when a file in the directory being watched gets modified in
// some way.
func (d *dirWatcher) Notify(name string) {
	d.considerFile(name, nil)
}
