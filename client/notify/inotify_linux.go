// COPYRIGHT

package notify

import (
	"fmt"
	"sync"
	"syscall"
	"unsafe"
)

// Initialize the notifier.
func init() {
	addNewNotify("inotify", 1000, newINotifyWatcher)
}

// This is a linux specific implementation of a file system watcher that uses
// inotify.
type iNotify struct {
	// A lock that protects all the fields in this object.
	lock sync.Mutex

	// The file descriptor to the inotify socket.
	fd int

	// A mapping of the watch id returned by inotify to the notifier object
	// that will get notified of inotify events.
	watches map[uint32]Callback

	// Set to true if this watcher is shutting down. Once this is set to true,
	// and the fd above is closed all further errors will be ignored and
	// the processing routine will terminate nearly immediately.
	stop bool
}

// Creates a new iNotify and returns the watcher interface for
// use by internal APIs.
func newINotifyWatcher() (Notify, error) {
	fd, err := syscall.InotifyInit1(syscall.IN_CLOEXEC)
	if err != nil {
		return nil, err
	}
	return &iNotify{
		fd:      fd,
		watches: make(map[uint32]Callback),
	}, nil
}

// Removes all watches that point to a given callback.
func (i *iNotify) RemoveWatch(c Callback) error {
	i.lock.Lock()
	defer i.lock.Unlock()
	for id, callback := range i.watches {
		if c == callback {
			delete(i.watches, id)
			if _, err := syscall.InotifyRmWatch(i.fd, id); err != nil {
				return err
			}
		}
	}
	return nil
}

// Runs the watcher routine. This will call out to any notifiers added
// via the AddWatch() call and will exit when Stop() is called or when an
// error happens.
func (i *iNotify) Run() error {
	// Make a buffer:
	var buffer [syscall.SizeofInotifyEvent * 4096]byte

	// Loop through reading events from the buffer.
	for !i.stop {
		n, err := syscall.Read(i.fd, buffer[0:])
		if n == 0 {
			return fmt.Errorf("The inotify socket was closed.")
		} else if err != nil {
			return err
		} else if n < 0 {
			return fmt.Errorf("Short read() from syscall.Read()")
		}

		// Loop through all of the items read from the Read() operation
		// above.
		for j := 0; j < n; {
			event := (*syscall.InotifyEvent)(unsafe.Pointer(&buffer[j]))
			j += int(syscall.SizeofInotifyEvent)
			start := j
			end := j
			j += int(event.Len)
			for i := uint32(0); i < event.Len; i++ {
				if buffer[end] == '\000' {
					break
				} else {
					end += 1
				}
			}
			fName := string(buffer[start:end])

			// See if this event is meaningful for one of our watches.
			i.lock.Lock()
			dest, ok := i.watches[uint32(event.Wd)]
			i.lock.Unlock()
			if ok {
				dest.Notify(fName)
			}
		}
	}

	// Success!
	return nil
}

// Stops the watcher. This will not wait for the running Run() call to
// complete before exiting. If the process has already been stopped then
// this will still succeed.
func (i *iNotify) Stop() {
	i.lock.Lock()
	defer i.lock.Unlock()
	i.stop = true
	for id, _ := range i.watches {
		delete(i.watches, id)
		syscall.InotifyRmWatch(i.fd, id)
	}
	syscall.Close(int(i.fd))
}

// Adds a new watch for a given directory.
func (i *iNotify) WatchDirectory(dir string, c Callback) error {
	i.lock.Lock()
	defer i.lock.Unlock()
	if i.fd < 0 {
		return fmt.Errorf("Notify has already been stopped.")
	}
	// We want to watch for:
	//   File creation
	//   File deletion
	//   File moved into.
	//   File modified.
	//   File opened in write mode closed.
	mask := uint32(0 |
		syscall.IN_CREATE |
		syscall.IN_DELETE |
		syscall.IN_MOVED_TO |
		syscall.IN_MODIFY |
		syscall.IN_CLOSE_WRITE)
	watchId, err := syscall.InotifyAddWatch(i.fd, dir, mask)
	if err != nil {
		return err
	}
	i.watches[uint32(watchId)] = c
	return nil
}
