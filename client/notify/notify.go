// COPYRIGHT

package notify

import (
	"fmt"
)

// FIXME
type Notify interface {
	// Runs the notification goroutine.
	Run() error

	// Stops the running notification goroutine.
	Stop()

	// Adds a watch on the given directory.
	WatchDirectory(dir string, c Callback) error

	// Removes all watches that point to the given Callback object.
	RemoveWatch(c Callback) error
}

// Creates a new Notify() object that uses the given type. Types are
// platform specific and can be discovered via the AvailableTypes()
// function call.
func NewNotifyByType(typ string) (Notify, error) {
	if n, ok := availableNotify[typ]; ok {
		return n()
	} else {
		return nil, fmt.Errorf("Not a valid notify type: %s", typ)
	}
}

// Returns a list of known notification implementations.
func NotificationImplementations() []string {
	ret := make([]string, 0, len(availableNotify))
	for name, _ := range availableNotify {
		ret = append(ret, name)
	}
	return ret
}

// Returns the name of the default Notification implementation.
func DefaultNotificationName() string {
	return bestNotifyName
}

// Creates a new Notify object using the best implementation available
// on the current system.
func NewNotify() (Notify, error) {
	if bestNotify == nil {
		return nil, fmt.Errorf("No valid notify types registered.")
	}
	return bestNotify()
}

// When a file has been created, modified, or deleted in the watched
// directory.
type Callback interface {
	Notify(file string)
}

// As each platform specific implementation is built this will be populated
// in order to allow automatic discovery of available Notify implementations.
var (
	availableNotify = map[string]func() (Notify, error){}
	bestNotify      = (func() (Notify, error))(nil)
	bestNotifyScore = int(0)
	bestNotifyName  string
)

// Internal function for adding Notify implementations.
func addNewNotify(name string, score int, n func() (Notify, error)) {
	if _, ok := availableNotify[name]; ok {
		panic(fmt.Sprintf(
			"A Notify implementation named %s already exists.",
			name))
	}
	if score > bestNotifyScore {
		bestNotify = n
		bestNotifyScore = score
		bestNotifyName = name
	}
	availableNotify[name] = n
}
