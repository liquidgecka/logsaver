// COPYRIGHT

package notify

import (
	"os"
	"path/filepath"
	"sync"
	"testing"
	"time"

	"github.com/liquidgecka/testlib"
)

type testingCallback struct {
	called bool
	file   string
}

func (t *testingCallback) Notify(file string) {
	t.called = true
	t.file = file
}

func TestINotify(t *testing.T) {
	T := testlib.NewT(t)
	defer T.Finish()

	// Create a watcher.
	notify, err := newINotifyWatcher()
	T.ExpectSuccess(err)
	T.NotEqual(notify, nil)

	// Start the watcher and ensure it stops properly on error.
	var runErr error
	wg := sync.WaitGroup{}
	wg.Add(1)
	defer wg.Wait()
	defer notify.Stop()
	go func() {
		runErr = notify.Run()
		wg.Done()
	}()

	// Make a temp directory to work with.
	tmpDir := T.TempDir()

	// Start by adding a new watch.
	cb := &testingCallback{}
	err = notify.WatchDirectory(tmpDir, cb)
	T.ExpectSuccess(err)

	// Make a change and see that it gets notified.
	fd, err := os.Create(filepath.Join(tmpDir, "test1"))
	T.ExpectSuccess(err)
	defer fd.Close()
	T.TryUntil(
		func() bool {
			time.Sleep(time.Millisecond)
			return cb.called && cb.file == "test1"
		},
		time.Second)

	// Write to the file and expect a notification.
	_, err = fd.WriteString("TEST")
	T.ExpectSuccess(err)
	cb.called = false
	cb.file = ""
	T.TryUntil(
		func() bool {
			time.Sleep(time.Millisecond)
			return cb.called && cb.file == "test1"
		},
		time.Second)

	// Now we can close the file and expect another call.
	cb.called = false
	cb.file = ""
	fd.Close()
	T.TryUntil(
		func() bool {
			time.Sleep(time.Millisecond)
			return cb.called && cb.file == "test1"
		},
		time.Second)

	// And finally we can delete the file and see.
	cb.called = false
	cb.file = ""
	err = os.Remove(filepath.Join(tmpDir, "test1"))
	T.ExpectSuccess(err)
	T.TryUntil(
		func() bool {
			time.Sleep(time.Millisecond)
			return cb.called && cb.file == "test1"
		},
		time.Second)

	// And finally, stop the watcher.
	notify.Stop()

	// Make sure that the Run routine has not errored.
	wg.Wait()
	T.ExpectSuccess(runErr)
}
