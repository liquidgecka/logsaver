// COPYRIGHT

package client

import (
	"fmt"
)

type File interface {
	// Returns the name of the file that is being processed.
	FileName() string

	// Actually processes the file. This will return an error if the
	// file has been deleted, or if there is an error talking with or
	// uploading the file.
	Run() error

	// Initiates a graceful shutdown of the File processor. This will allow
	// the processor to finish uploading the file to the remote server
	// before terminating the processor.
	Shutdown()

	// Forces the processor to stop immediately terminate without finishing
	// processing the file.
	Stop()

	// A trigger that lets the File processor that the file has been modified
	// on disk and should be re-evaluated.
	Updated()
}

// A list of the valid types of File initializers.
var (
	validTypes = map[string]func(string, string, string, *Config) File{}
)

// adds a new file type to the configured map.
func addFileType(n string, f func(string, string, string, *Config) File) {
	if _, ok := validTypes[n]; ok {
		panic(fmt.Sprintf("File type '%s' defined more than once.", n))
	}
	validTypes[n] = f
}

// Returns a list of valid file types.
func ValidFileTypes() []string {
	ret := make([]string, 0, len(validTypes))
	for name, _ := range validTypes {
		ret = append(ret, name)
	}
	return ret
}

// Creates a new File() object of the given type. If the type is not valid
// then this will return a nil object.
func NewFile(typ, base, file, url string, c *Config) File {
	if v, ok := validTypes[typ]; !ok {
		panic(fmt.Sprintf("Unsupported file type: %s", typ))
		return nil
	} else {
		return v(base, file, url, c)
	}
}
