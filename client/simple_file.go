// COPYRIGHT

package client

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func init() {
	addFileType("simple", newSimpleFile)
}

// Simple files are files which do not get rotated or moved in any way. The
// name that they have on disk will remain the same through the life of the
// file. This is the easiest case for file management since nothing needs to
// be done to manage file tracking at all.
type simpleFile struct {
	// Import the common functionality in the commonFile struct.
	commonFile

	// The interval at which the file will be manually checked rather than
	// relying on notification updates.
	manualCheckInterval time.Duration
}

func newSimpleFile(base, file, url string, c *Config) File {
	s := &simpleFile{
		manualCheckInterval: c.ManualFileCheckInterval,
	}
	if !strings.HasSuffix(url, "/") {
		s.init(filepath.Join(base, file), url+"/"+file, c)
	} else {
		s.init(filepath.Join(base, file), url+file, c)
	}
	return s
}

func (s *simpleFile) Run() error {
	// Start off by making sure that we get the status of the file on the
	// remote logsaver so that we know where to start uploading from.
	if err := s.getRemoteStatus(); err != nil {
		// FIXME: logging?
		return err
	}

	// This returns the current status of the file for use in the
	// for loop. The first value is true if the loop should terminate.
	// The cases where this will be true are:
	//   1 - Hard stop.
	//   2 - Graceful shutdown and the file is caught up.
	// The second value is true if the file has updates that need to be
	// processed.
	status := func() (stop bool, upload bool) {
		upload = s.remoteSize < s.localSize
		if s.context.Err() == context.Canceled {
			return true, upload
		}
		s.lock.Lock()
		ret := s.shutdown && s.remoteSize == s.localSize
		s.lock.Unlock()
		return ret, upload
	}

	// Main processing loop.
	for {
		// Stat the file and see if it has changed locally.
		stat, err := os.Stat(s.file)
		if err != nil {
			if os.IsNotExist(err) {
				// The file was removed, check to see if it was caught up
				// as of our last check. If so we can simply forget it.
				if s.remoteSize == s.localSize {
					// FIXME: Log something?
					return nil
				} else {
					// FIXME: Log something!
					s.lock.Lock()
					defer s.lock.Unlock()
					return fmt.Errorf(""+
						"File removed before is was fully uploaded. "+
						"Only %d of %d bytes (%0.1f%%) were uploaded.",
						s.localSize,
						s.remoteSize,
						float64(s.remoteSize)/float64(s.localSize)*100.0)
				}
			} else {
				// FIXME: Log something!
				return err
			}
		}

		// Get the status and see if we need to take action or sleep and try
		// again later.
		stop, upload := status()
		if stop {
			break
		} else if !upload {
			// Sleep for the manual check interval before performing
			// another check. We also bail out on notification updates
			// since that means that the file has changed in some way.
			timer := time.NewTimer(s.manualCheckInterval)
			select {
			case <-timer.C:
			case <-s.updatedChan:
				if !timer.Stop() {
					<-timer.C
				}
			case <-s.context.Done():
				if !timer.Stop() {
					<-timer.C
				}
				return fmt.Errorf("Aborted.")
			}
		}

		// Store the local size that we just read from disk.
		s.lock.Lock()
		s.localSize = stat.Size()
		upload = s.localSize > s.remoteSize
		s.lock.Unlock()

		// If the file has not actually changed then we need to sleep for
		// the manual check interval and then try again.
		if !upload {
		}

		// Open the file so we can pass it to the uploader.
		if fd, err := os.Open(s.file); err != nil {
			// FIXME
			return err
		} else if err := s.uploadData(fd); err != nil {
			// FIXME
			return err
		}
	}

	// Success.
	return nil
}
