// COPYRIGHT

package client

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"gitlab.com/liquidgecka/logsaver/client/notify"
	"gitlab.com/liquidgecka/logsaver/proto"
)

type commonFile struct {
	// The full path to the file on disk that is being processed.
	file string

	// Each file gets a unique URL to a remote path for this file.
	// This ensures that files shouldn't ever end up writing to the
	// same destination.
	remoteURL string

	// Settings used during http calls.
	backoff                time.Duration
	httpClient             *http.Client
	maximumBytesPerRequest int64
	maximumRetries         int
	processingTimeout      time.Duration
	requestTimeout         time.Duration

	// The notification object.
	notify notify.Notify

	// A lock used for protecting internal variables that get updated
	// during the regular running of this structure.
	lock sync.Mutex

	// The last seen local size of this file (zero if not known).
	localSize int64

	// The last reported remote size of this file (zero if not known).
	remoteSize int64

	// The modification time of this file on disk. This is used to
	// handle deleting old files.
	modTime time.Time

	// Set to true if the file has bee notified that is has updated
	// on disk.
	updated     bool
	updatedChan chan struct{}

	// A context and its cancel function that will be used for controlling
	// start/stop functionality with this file object. If this gets canceled
	// then all further processing will be stopped and any running
	// goroutines will return.
	context context.Context
	cancel  func()

	// Set to true if the file should gracefully shutdown.
	shutdown     bool
	shutdownChan chan struct{}
}

// Initialize the common file object.
func (c *commonFile) init(file, url string, conf *Config) {
	c.file = file
	c.remoteURL = url
	c.backoff = conf.Backoff
	c.httpClient = conf.HTTPClient
	c.maximumBytesPerRequest = conf.MaximumBytesPerRequest
	c.maximumRetries = conf.MaximumRetries
	c.processingTimeout = conf.ProcessingTimeout
	c.requestTimeout = conf.RequestTimeout
	c.notify = conf.Notify
	c.localSize = -1
	c.remoteSize = -1
	c.updated = false
	c.updatedChan = make(chan struct{})
	c.shutdown = false
	c.shutdownChan = make(chan struct{})
	ctx := conf.ParentContext
	if ctx == nil {
		ctx = context.Background()
	}
	c.context, c.cancel = context.WithCancel(ctx)
}

// Returns the file name being processed.
func (c *commonFile) FileName() string {
	return c.file
}

// Notifies this file object that the underlying file has changed in some
// way.
func (c *commonFile) Updated() {
	c.lock.Lock()
	c.updated = true
	c.lock.Unlock()
	select {
	case c.updatedChan <- struct{}{}:
	default:
	}
}

// Handler for the Notify interface.
func (c *commonFile) Notify(f string) {
	c.Updated()
}

// Initiates shutdown of the File object.
func (c *commonFile) Shutdown() {
	c.lock.Lock()
	c.shutdown = true
	c.lock.Unlock()
}

// Forces the File to stop.
func (c *commonFile) Stop() {
	c.lock.Lock()
	c.shutdown = true
	c.lock.Unlock()
	c.cancel()
}

// Gets the status of the file on the remote server.
func (c *commonFile) getRemoteStatus() error {
	requestFunc := func() (*http.Request, error) {
		return http.NewRequest("GET", c.remoteURL, nil)
	}
	responseFunc := func(r *http.Response) error {
		if r.StatusCode != 200 {
			return fmt.Errorf("Non 200 status code.")
		}
		body, err := ioutil.ReadAll(&io.LimitedReader{
			R: r.Body,
			N: 1024 * 16,
		})
		if err != nil {
			return err
		}

		// JSON decode the results.
		var data *proto.HeadResponse
		if err := json.Unmarshal(body, &data); err != nil {
			return err
		}
		c.lock.Lock()
		c.remoteSize = int64(data.Size)
		c.lock.Unlock()
		return nil
	}
	if err := c.request(requestFunc, responseFunc); err != nil {
		return err
	}
	return nil
}

// A wrapper around http.Request that will keep retrying the request until
// the processing timeout or retry count has been exceeded. This takes two
// functions, 'rf' generates a *http.Request object for use with the request,
// and 'c' checks the http.Response object to see if it is valid. If the
// request is unable to be completed within the deadline period then
// this will return an error.
func (c *commonFile) request(
	rf func() (*http.Request, error),
	w func(*http.Response) error,
) error {
	// Setup a context that will timeout after ProcessingTimeout expires.
	ctx, _ := context.WithTimeout(
		c.context,
		c.processingTimeout)

	// We want to track all of the errors that happened during the
	// request life cycle so that we can properly report the errors to
	// the logger.
	errors := make([]error, 0, c.maximumRetries+1)

	// Ensure that we process for the maximum number of retries.
	for retries := 0; retries < c.maximumRetries; retries += 1 {
		fmt.Printf("TRYING... %d\n", c.maximumRetries)
		//c.config.Logger.Debugf(
		//	"[%s] Attempting to make a HTTP request (try %d).",
		//	c.ID,
		//	retries+1)

		// Check to see if the processing context has expired. If so then
		// we can return an error.
		select {
		case <-ctx.Done():
			errors = append(errors, fmt.Errorf(
				"ProcessingTimeout (%s) reached",
				c.processingTimeout))
			break
		default:
		}

		// Get a request object from the caller and make a context for this
		// single http call to live in.
		request, err := rf()
		if err != nil {
			//c.config.Logger.Debugf(
			//	"[%s] Error generating HTTP request: %s",
			//	c.ID,
			//	err)
			errors = append(errors, err)
			continue
		}
		reqContext, _ := context.WithTimeout(ctx, c.requestTimeout)
		request = request.WithContext(reqContext)

		// FIXME: Authentication data?

		// Make the actual HTTP call.
		response, err := c.httpClient.Do(request)
		if err == nil {
			if err = w(response); err == nil {
				// Success! Make sure we read the full request body
				// so that the http session can be reused.
				if response != nil && response.Body != nil {
					io.Copy(ioutil.Discard, response.Body)
				}
				//c.config.Logger.Debugf(
				//	"[%s] Success making HTTP request.",
				//	c.ID)
				return nil
			} else {
				//c.config.Logger.Debugf(
				//	"[%s] Error processing response: %s",
				//	c.ID,
				//	err)
				errors = append(errors, err)
			}
		} else {
			//c.config.Logger.Debugf(
			//	"[%s] Error making HTTP response: %s",
			//	c.ID,
			//	err)
			errors = append(errors, err)
		}

		// Calculate how much time we should spend backing off. This is a
		// short sleep in order to prevent a thundering heard issue on the
		// logsaver server.
		if retries > 0 {
			backoff := c.backoff * time.Duration(retries)
			//c.config.Logger.Debugf(
			//	"[%s] Backing off %s before trying again.",
			//	c.ID,
			//	backoff)
			timer := time.NewTimer(backoff)
			select {
			case <-timer.C:
			case <-c.context.Done():
				if !timer.Stop() {
					<-timer.C
				}
				return fmt.Errorf("Operation aborted due to stop()")
			}
		}
	}

	// FIXME: logging
	errorMsgs := make([]string, len(errors))
	for i, err := range errors {
		errorMsgs[i] = err.Error()
	}
	return fmt.Errorf(
		"Too many faild requests: %s",
		strings.Join(errorMsgs, ", "))
}

// Uploads a chunk of data to the remote server. This is used when the file
// expands and the new data has not been uploaded yet.
func (c *commonFile) uploadData(fd *os.File) error {
	// Get the remote file size so we can start reading from that offset.
	c.lock.Lock()
	start := c.remoteSize
	c.lock.Unlock()

	// Start off by seeking to the last point that the remote server has.
	if _, err := fd.Seek(start, io.SeekStart); err != nil {
		return err
	}
	data, err := ioutil.ReadAll(
		&io.LimitedReader{
			R: fd,
			N: c.maximumBytesPerRequest,
		})
	if err != nil {
		//c.config.Logger.Debugf(
		//	"[%s] Error reading from the file: %s",
		//	c.ID,
		//	err)
		return err
	}

	// Setup the request and response functions.
	requestFunc := func() (*http.Request, error) {
		r, err := http.NewRequest("PUT", c.remoteURL, bytes.NewBuffer(data))
		if err == nil {
			r.Header.Add(
				"Range",
				fmt.Sprintf("%d-%d", start, start+int64(len(data))))
		}
		return r, err
	}
	responseFunc := func(r *http.Response) error {
		if r.StatusCode != http.StatusNoContent {
			body, _ := ioutil.ReadAll(r.Body)
			return fmt.Errorf(
				"Bad response from the server (%d): `%s`",
				r.StatusCode,
				string(body))
		}
		return nil
	}
	if err := c.request(requestFunc, responseFunc); err != nil {
		return err
	}

	// We uploaded the bytes and the server accepted them, now we
	// can increment the remote size accordingly.
	c.lock.Lock()
	c.remoteSize += int64(len(data))
	c.lock.Unlock()
	//c.config.Logger.Debugf(
	//	"[%s] Successfully uploaded %d bytes, the remote file is %d bytes.",
	//	len(data),
	//	remoteSize)

	// Success!
	return nil
}
