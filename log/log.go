package log

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"sync"
	"sync/atomic"
	"time"
)

type Level int32

const (
	Trace = Level(iota)
	Debug
	Info
	Warning
	Error
	Fatal
)

func (l Level) String() string {
	switch Level(atomic.LoadInt32((*int32)(&l))) {
	case Trace:
		return "TRACE"
	case Debug:
		return "DEBUG"
	case Info:
		return " INFO"
	case Warning:
		return " WARN"
	case Error:
		return "ERROR"
	case Fatal:
		return "FATAL"
	default:
		return "INVLD"
	}
}

type Log struct {
	out   *os.File
	lock  sync.Mutex
	level Level
}

func New() *Log {
	return &Log{
		out:   os.Stdout,
		level: Info,
	}
}

func writeHeader(l Level, out io.Writer) {
	now := time.Now()
	fmt.Fprintf(out, "%s [%04d/%02d/%02d-%02d:%02d:%02d.%03d]: ",
		l.String(), now.Year(), now.Month(), now.Day(), now.Hour(),
		now.Minute(), now.Second(), now.Nanosecond()/1000000)
}

func (l *Log) More() {
	atomic.AddInt32((*int32)(&l.level), -1)
}

func (l *Log) Tracef(f string, args ...interface{}) {
	if atomic.LoadInt32((*int32)(&l.level)) > int32(Trace) {
		return
	}
	buffer := &bytes.Buffer{}
	buffer.Grow(len(f)*2 + 40)
	writeHeader(Trace, buffer)
	fmt.Fprintf(buffer, f, args...)
	fmt.Fprintf(buffer, "\n")
	l.lock.Lock()
	defer l.lock.Unlock()
	io.Copy(l.out, buffer)
}

func (l *Log) Debugf(f string, args ...interface{}) {
	if atomic.LoadInt32((*int32)(&l.level)) > int32(Debug) {
		return
	}
	buffer := &bytes.Buffer{}
	buffer.Grow(len(f)*2 + 40)
	writeHeader(Debug, buffer)
	fmt.Fprintf(buffer, f, args...)
	fmt.Fprintf(buffer, "\n")
	l.lock.Lock()
	defer l.lock.Unlock()
	io.Copy(l.out, buffer)
}

func (l *Log) Infof(f string, args ...interface{}) {
	if atomic.LoadInt32((*int32)(&l.level)) > int32(Info) {
		return
	}
	buffer := &bytes.Buffer{}
	buffer.Grow(len(f)*2 + 40)
	writeHeader(Info, buffer)
	fmt.Fprintf(buffer, f, args...)
	fmt.Fprintf(buffer, "\n")
	l.lock.Lock()
	defer l.lock.Unlock()
	io.Copy(l.out, buffer)
}

func (l *Log) Warningf(f string, args ...interface{}) {
	if atomic.LoadInt32((*int32)(&l.level)) > int32(Warning) {
		return
	}
	buffer := &bytes.Buffer{}
	buffer.Grow(len(f)*2 + 40)
	writeHeader(Warning, buffer)
	fmt.Fprintf(buffer, f, args...)
	fmt.Fprintf(buffer, "\n")
	l.lock.Lock()
	defer l.lock.Unlock()
	io.Copy(l.out, buffer)
}

func (l *Log) Errorf(f string, args ...interface{}) {
	if atomic.LoadInt32((*int32)(&l.level)) > int32(Error) {
		return
	}
	buffer := &bytes.Buffer{}
	buffer.Grow(len(f)*2 + 40)
	writeHeader(Error, buffer)
	fmt.Fprintf(buffer, f, args...)
	fmt.Fprintf(buffer, "\n")
	l.lock.Lock()
	defer l.lock.Unlock()
	io.Copy(l.out, buffer)
}

func (l *Log) Fatalf(f string, args ...interface{}) {
	if atomic.LoadInt32((*int32)(&l.level)) > int32(Fatal) {
		return
	}
	buffer := &bytes.Buffer{}
	buffer.Grow(len(f)*2 + 40)
	writeHeader(Fatal, buffer)
	fmt.Fprintf(buffer, f, args...)
	fmt.Fprintf(buffer, "\n")
	l.lock.Lock()
	defer l.lock.Unlock()
	io.Copy(l.out, buffer)
	l.out.Sync()
	os.Exit(1)
}
