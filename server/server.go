// COPYRIGHT

package server

import (
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/liquidgecka/logsaver/log"
)

// Configuration options for the NewServer call.
type Config struct {
	Logger  *log.Log
	Port    int
	BaseDir string
}

type Server interface {
	// Runs the server. This will not return until the server has been
	// stopped or shut down.
	Run() error

	// Initiates a full stop of the process. When called this will attempt
	// to kill off all running routines as quickly as possible.
	Stop()
}

func NewServer(conf *Config) (Server, error) {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", conf.Port))
	if err != nil {
		return nil, err
	}
	svr := &server{
		listener:            listener,
		logger:              conf.Logger,
		rootLogDirectory:    conf.BaseDir,
		logLongUnlockWindow: time.Second * 30, // FIXME: Parameterize
		processingFiles:     make(map[string]bool, 1000),
	}
	svr.processingFilesCond.L = new(sync.Mutex)
	svr.httpServer = &http.Server{
		Handler:           svr,
		ReadTimeout:       time.Second * 30, // FIXME: Parameterize
		WriteTimeout:      time.Second * 30, // FIXME: Parameterize
		ReadHeaderTimeout: time.Second * 5,  // FIXME: Parameterize
		IdleTimeout:       time.Minute * 5,  // FIXME: Parameterize
		MaxHeaderBytes:    2 << 16,
	}
	return svr, nil
}

type server struct {
	// The underlying TCP listener than is being used to serve.
	listener net.Listener

	// The http.Server that is actually serving requests from clients.
	httpServer *http.Server

	// A writer that receive log files.
	log    io.WriteCloser
	logger *log.Log

	// The root log directory
	rootLogDirectory string

	// Tracks incoming requests. This is used to ensure that every request
	// has a unique number associated with it.
	requestCounter int64

	// A list of all files currently being processed. This is used to ensure
	// that two operations don't happen on the same file at the same time.
	processingFilesCond sync.Cond
	processingFiles     map[string]bool

	// Like above this protects directory creation. We could get in a situation
	// where two routines are racing to create the same directory tree.
	directoryCreationLock sync.Mutex

	// If a lock in the processingFiles map takes longer than this then
	// the server will log. This is useful for detecting if the server is
	// getting overloaded.
	logLongUnlockWindow time.Duration
}

func (s *server) Run() error {
	return s.httpServer.Serve(s.listener)
}

func (s *server) Stop() {
	s.listener.Close()
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	requestId := atomic.AddInt64(&s.requestCounter, 1)
	altWriter := &captureHTTPResponse{Writer: w}
	startTime := time.Now()

	// Start by seeing what kind of request this is.
	if r.Method == "GET" || r.Method == "" {
		s.requestGET(requestId, altWriter, r)
	} else if r.Method == "PUT" {
		s.requestPUT(requestId, altWriter, r)
	} else {
		http.Error(altWriter, "Method not allowed.",
			http.StatusMethodNotAllowed)
	}

	// Log the request
	if s.log != nil {
		logStr := altWriter.Log(r, startTime)
		io.WriteString(s.log, logStr)
	}
}

// This is the HTTP handler for GET requests. This is used when a client
// requests information about a file.
func (s *server) requestGET(
	requestId int64, w http.ResponseWriter, r *http.Request,
) {
	s.logger.Debugf("[%d] GET request for %s initiated.",
		requestId, r.URL.Path)

	// Get the filename, if this call returns an empty string then the
	// error was already served. This is a bit odd but it helps
	// reduce code.
	fn := s.getFilename(requestId, w, r)
	if fn == "" {
		return
	}

	// Next we get the size of the file.
	size, err := s.getFileData(requestId, fn)
	if os.IsNotExist(err) {
		s.logger.Debugf("[%d] File not found: %s", requestId, fn)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(fmt.Sprintf(`{"size": 0}`)))
		return
	} else if err != nil {
		s.logger.Warningf("[%d] Error retrieving file details for %s: %s",
			requestId, fn, err.Error())
		http.Error(w, "Error retrieving file details.",
			http.StatusInternalServerError)
		return
	}

	// Okay, everything looks golden, now we can return the status data on the
	// file.
	s.logger.Debugf("[%d] Success, %s is %d bytes long.", requestId, fn, size)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(fmt.Sprintf(`{"size": %d}`, size)))
}

// This is the HTTP Handler for PUT requests.
func (s *server) requestPUT(
	requestId int64, w http.ResponseWriter, r *http.Request,
) {
	s.logger.Debugf("[%d] PUT request for %s initiated.",
		requestId, r.URL.Path)

	// Get the filename, if this call returns an empty string then the
	// error was already served. This is a bit odd but it helps
	// reduce code.
	fn := s.getFilename(requestId, w, r)
	if fn == "" {
		return
	}

	// Parse the Range: header to see what range we should be working with.
	if ranges, ok := r.Header["Range"]; !ok {
		http.Error(w, "Missing the Range header.\n", http.StatusBadRequest)
		return
	} else if len(ranges) != 1 {
		http.Error(
			w,
			"Too many Range headers provided.\n",
			http.StatusBadRequest)
		return
	} else if parts := strings.Split(ranges[0], "-"); len(parts) != 2 {
		http.Error(
			w,
			"Invalid Range header.\n",
			http.StatusBadRequest)
		return
	} else if start, err := strconv.ParseInt(parts[0], 10, 63); err != nil {
		http.Error(
			w,
			fmt.Sprintf(
				"Error parsing the range header: %s",
				err.Error()),
			http.StatusBadRequest)
		return
	} else if end, err := strconv.ParseInt(parts[1], 10, 63); err != nil {
		http.Error(
			w,
			fmt.Sprintf("Error parsing the range header: %s", err.Error()),
			http.StatusBadRequest)
		return
	} else if data, err := ioutil.ReadAll(r.Body); err != nil {
		s.logger.Infof("[%d] Error reading request body: %s",
			requestId, err.Error())
		http.Error(
			w,
			fmt.Sprintf("Error reading request body: %s", err.Error()),
			http.StatusInternalServerError)
		return
	} else if int64(len(data)) != end-start {
		http.Error(
			w,
			fmt.Sprintf(
				"Invalid number of bytes read, expected %d, got %d\n",
				end-start,
				len(data)),
			http.StatusBadRequest)
		return
	} else if err := s.appendData(requestId, fn, start, data); err != nil {
		s.logger.Infof("[%d] Error appending data: %s", requestId, err.Error())
		http.Error(w, fmt.Sprintf(
			"Error writing data to the log file: %s",
			err.Error()),
			http.StatusServiceUnavailable)
		return
	}

	// Success
	w.WriteHeader(http.StatusNoContent)
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte{})
}

// Converts a Path into a filename. If the path is invalid, or falls outside
// of the logging directory then this will serve an error and then return
// an empty string.
func (s *server) getFilename(
	requestId int64, w http.ResponseWriter, r *http.Request,
) string {
	// Get the real on disk filename that we should be using.
	fn := filepath.Join(s.rootLogDirectory, r.URL.Path)
	fn, err := filepath.Abs(fn)
	if err != nil {
		s.logger.Warningf(
			"[%d] Error converting the path (%s) into a file name: %s",
			requestId, r.URL.Path, err.Error())
		http.Error(w, "Bad path value.\n", http.StatusBadRequest)
		return ""
	}

	// If the path the client is attempting to write to is not in the log
	// directory then we have to reject it. This can be done if the caller
	// used something like "../../etc/passwd" or some such.
	if !strings.HasPrefix(fn, s.rootLogDirectory) {
		s.logger.Warningf(
			"[%d] Client %s tried to access a restricted path: %s",
			requestId, r.RemoteAddr, fn)
		http.Error(w, "Invalid path value.\n", http.StatusBadRequest)
		return ""
	}

	// Success!
	return fn
}

// This is the inner call that gets metadata about a given file.
func (s *server) getFileData(
	requestId int64, fn string) (int64, error,
) {
	// Start by locking the filename specific lock so that no other routine
	// will attempt to write to this file while we are already working on it.
	closer := s.obtainFileLock(requestId, fn)
	defer closer()

	// Attempt to stat the file.
	if stat, err := os.Stat(fn); err != nil {
		// A special case here is that the file didn't exist, in which case
		// we return a 0 and success. That lets the client know that they can
		// upload the file from the beginning which will create the file here.
		if os.IsNotExist(err) {
			return 0, nil
		}
		return 0, err
	} else {
		return stat.Size(), nil
	}
}

// This function is called in order to acquire the lock on the given filename.
// It will not return until the it can obtain an exclusive lock on the file,
// and once the function finishes it needs to call the returned function to
// unlock the map.
func (s *server) obtainFileLock(requestId int64, fn string) func() {
	closer := func() {
		s.processingFilesCond.L.Lock()
		defer s.processingFilesCond.L.Unlock()
		delete(s.processingFiles, fn)
		s.processingFilesCond.Broadcast()
	}

	// The starting time.
	warnTime := time.Now().Add(s.logLongUnlockWindow)

	// Make sure that we are locked.
	s.processingFilesCond.L.Lock()
	defer s.processingFilesCond.L.Unlock()

	for {
		// Case one, if the filename didn't exist in the map at all then its
		// safe to add it and return since nothing can be waiting on it.
		if _, ok := s.processingFiles[fn]; !ok {
			s.processingFiles[fn] = true
			return closer
		}

		// Check to see if we have delayed too long.
		if time.Now().After(warnTime) {
			s.logger.Warningf(
				"[%d] has taken too long to obtain a file lock.",
				requestId)
			warnTime = time.Now().Add(s.logLongUnlockWindow)
		}

		// The item already exists, we can not continue because some other
		// thread is already processing the file.
		s.processingFilesCond.Wait()
	}
}

// This is the inner function that actually appends data to a file. It
// provides several major safety checks, primarily the file size gate, and
// the filename gate. This will also create directories in the root log
// directory as needed in order to ensure that files can be created.
// On error this returns an error object describing the issue.
//
// Note that this function assumes that the file name has already been
// sanitized. That should be done at the HTTP request later, far before
// this function is ever called.
func (s *server) appendData(
	requestId int64, fn string, offset int64, data []byte,
) error {
	// First step is to lock the map so that we can be sure that no other
	// goroutine will attempt to append data to the file while we are working.
	// We actually want to defer the closer until the end of the function as
	// well.
	closer := s.obtainFileLock(requestId, fn)
	defer closer()

	// At this point we are ready to start processing the request. Step one
	// is to open the file so we can stat it, but we also want to open in
	// creation mode if the offset is 0 (otherwise we want to fail).
	perms := os.O_APPEND | os.O_WRONLY
	if offset == 0 {
		perms += os.O_CREATE
	}
	s.logger.Debugf("[%d] Opening file: %s", requestId, fn)
	fd, err := os.OpenFile(fn, perms, 0644)
	if offset == 0 && os.IsNotExist(err) {
		if err := s.mkDirs(requestId, fn); err == nil {
			fd, err = os.OpenFile(fn, perms, 0644)
		}
	}
	if err != nil {
		return err
	}
	defer func() {
		if err := fd.Close(); err != nil {
			s.logger.Warningf(
				"[%d] Error closing %s (this error is ignored): %s",
				requestId, fn, err.Error())
		}
	}()

	// State the file and ensure that its the right size for this append.
	if stat, err := fd.Stat(); err != nil {
		return err
	} else if stat.Size() != offset {
		return fmt.Errorf("[%d] Expected offset (%d) didn't match "+
			"the real file size (%d).", requestId, offset, stat.Size())
	}

	// Next we need to seek to the right place in the file.
	if n, err := fd.Seek(offset, 0); err != nil {
		return err
	} else if n != offset {
		s.logger.Warningf("[%d] Short seek, expected %d but got %d.",
			requestId, offset, n)
		return fmt.Errorf("Seek failed to land at the right place.")
	}

	// Okay, at this point we have the file exclusively opened, and it is
	// the right size to accept this write. We can go ahead and write the
	// data now.
	if n, err := fd.Write(data); err != nil {
		return err
	} else if n != len(data) {
		s.logger.Warningf("[%d] Short write, tried to write %d bytes but "+
			"actually wrote %d.", requestId, len(data), n)
		return fmt.Errorf("Short write, %d of %d bytes written.",
			n, len(data))
	}

	// Success! There is nothing else to do at this point.
	return nil
}

// Makes all the directories necessary to write a file.
func (s *server) mkDirs(requestId int64, fn string) error {
	s.logger.Debugf("[%d] Obtaining the directory creation lock.", requestId)
	s.directoryCreationLock.Lock()
	defer s.directoryCreationLock.Unlock()

	s.logger.Debugf("[%d] Making directories necessary for %s",
		requestId, fn)
	paths := strings.Split(fn, string(os.PathSeparator))
	for i := 0; i < len(paths); i++ {
		dir := string(os.PathSeparator) + filepath.Join(paths[0:i]...)
		if !strings.HasPrefix(dir, s.rootLogDirectory) {
			s.logger.Debugf("[%d] Ignoring %s (its not under the root)\n",
				requestId, dir)
			continue
		}
		s.logger.Debugf("[%d] Attempting to create %d", requestId, dir)
		err := os.Mkdir(dir, 0755)
		if err != nil && os.IsNotExist(err) {
			return err
		}
	}

	// Success!
	return nil
}
