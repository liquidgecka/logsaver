// COPYRIGHT

package server

type doOnceError struct {
	error
}

func DoOnceError(err error, fnc func()) error {
	if _, ok := err.(*doOnceError); ok {
		return err
	} else {
		fnc()
		return &doOnceError{error: err}
	}
}
