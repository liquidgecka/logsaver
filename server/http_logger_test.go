// COPYRIGHT

package server

import (
	"net/http"
	"testing"
	"time"

	"github.com/liquidgecka/testlib"
)

type responseWriterImpl struct {
	header  http.Header
	written []byte
	code    int
}

func (r *responseWriterImpl) Header() http.Header {
	return r.header
}

func (r *responseWriterImpl) Write(data []byte) (int, error) {
	r.written = data
	return len(r.written), nil
}

func (r *responseWriterImpl) WriteHeader(code int) {
	r.code = code
}

func Test_CaptureHTTPResponse_Header(t *testing.T) {
	T := testlib.NewT(t)
	defer T.Finish()

	rwi := &responseWriterImpl{}
	c := &captureHTTPResponse{Writer: rwi}
	T.Equal(c.Header(), c.Writer.Header())
	rwi.header = make(map[string][]string)
	T.Equal(c.Header(), c.Writer.Header())
}

func Test_CaptureHTTPResponse_Write(t *testing.T) {
	T := testlib.NewT(t)
	defer T.Finish()

	rwi := &responseWriterImpl{}
	c := &captureHTTPResponse{Writer: rwi}

	// Test 1: CaptureBody is False
	c.CaptureBody = false
	data := []byte{1, 2, 3, 4}
	i, err := c.Write(data)
	T.ExpectSuccess(err)
	T.Equal(i, len(data))
	T.Equal(rwi.written, data)
	T.Equal(c.BodyLen, int64(len(data)))
	T.Equal(c.Body, nil)

	// Test 2: CaptureBody is True
	c.CaptureBody = true
	data2 := []byte{5, 6, 7, 8, 9}
	i, err = c.Write(data2)
	T.ExpectSuccess(err)
	T.Equal(i, len(data2))
	T.Equal(rwi.written, data2)
	T.Equal(c.BodyLen, int64(len(data)+len(data2)))
	T.Equal(c.Body, data2)
}

func Test_CaptureHTTPResponse_Log(t *testing.T) {
	T := testlib.NewT(t)
	defer T.Finish()

	rwi := &responseWriterImpl{}
	c := &captureHTTPResponse{Writer: rwi}
	T.Equal(c.Status, 0)
	T.Equal(rwi.code, 0)
	c.WriteHeader(599)
	T.Equal(c.Status, 599)
}

func Test_CaptureHTTPResponse_WriteHeader(t *testing.T) {
	T := testlib.NewT(t)
	defer T.Finish()

	expectedDate := time.Unix(0, 0).Format("2/Jan/2006:15:04:05 -0700")

	rwi := &responseWriterImpl{}
	c := &captureHTTPResponse{Writer: rwi}
	c.BodyLen = 999

	// Fairly simple log line.
	startTime := time.Unix(0, 0)
	request, err := http.NewRequest("METHOD", "http://host:port/path", nil)
	T.ExpectSuccess(err)
	request.RequestURI = "/path"
	request.RemoteAddr = "1.2.3.4:1000"
	line := c.Log(request, startTime)
	T.Equal(line, `1.2.3.4:1000 - - [`+expectedDate+`] "METHOD /path" 200 999 "-" "-"`)

	// Add a user
	request.SetBasicAuth("user", "pass")
	line = c.Log(request, startTime)
	T.Equal(line, `1.2.3.4:1000 - user [`+expectedDate+`] "METHOD /path" 200 999 "-" "-"`)

	// Add a referer.
	request.Header.Add("Referer", "REFERER")
	line = c.Log(request, startTime)
	T.Equal(line, `1.2.3.4:1000 - user [`+expectedDate+`] "METHOD /path" 200 999 "REFERER" "-"`)

	// Add a user-agent
	request.Header.Add("User-Agent", "USER-AGENT")
	line = c.Log(request, startTime)
	T.Equal(line, `1.2.3.4:1000 - user [`+expectedDate+`] "METHOD /path" 200 999 "REFERER" "USER-AGENT"`)

	// Add a status
	c.Status = 998
	line = c.Log(request, startTime)
	T.Equal(line, `1.2.3.4:1000 - user [`+expectedDate+`] "METHOD /path" 998 999 "REFERER" "USER-AGENT"`)
}
