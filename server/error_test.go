// COPYRIGHT

package server

import (
	"fmt"
	"testing"

	"github.com/liquidgecka/testlib"
)

func Test_DoOnceError(t *testing.T) {
	T := testlib.NewT(t)
	defer T.Finish()

	firstErr := fmt.Errorf("error")

	// Test 1: First case firing of the error.
	didRun := false
	secondErr := DoOnceError(firstErr, func() { didRun = true })
	T.Equal(didRun, true)
	T.NotEqual(firstErr, secondErr)

	// Test 2: Second case attempt on the error.
	didRun = false
	thirdErr := DoOnceError(secondErr, func() { didRun = true })
	T.Equal(didRun, false)
	T.Equal(secondErr, thirdErr)
}
