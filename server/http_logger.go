// COPYRIGHT

package server

import (
	"fmt"
	"net/http"
	"time"
)

// When logging from golang's net/http Server its impossible to get some fields
// that are necessary in order to duplicate a nginx style log file. This
// captures the necessary fields in order to make a log file.
type captureHTTPResponse struct {
	Writer      http.ResponseWriter
	CaptureBody bool
	Status      int
	Body        []byte
	BodyLen     int64
}

// Wrapper for http.ResponseWriter
func (c *captureHTTPResponse) Header() http.Header {
	return c.Writer.Header()
}

// Wrapper for http.ResponseWriter
func (c *captureHTTPResponse) Write(d []byte) (int, error) {
	if c.CaptureBody {
		c.Body = append(c.Body, d...)
	}
	c.BodyLen += int64(len(d))
	return c.Writer.Write(d)
}

// Wrapper for http.ResponseWriter
func (c *captureHTTPResponse) WriteHeader(status int) {
	c.Status = status
	c.Writer.WriteHeader(status)
}

// Returns a textual log line that can be written to an access log. This
// format should mirror a default nginx log as closely as possible.
func (c *captureHTTPResponse) Log(r *http.Request, startTime time.Time) string {
	user, _, ok := r.BasicAuth()
	if !ok {
		user = "-"
	}
	timestamp := startTime.Format("2/Jan/2006:15:04:05 -0700")
	request := fmt.Sprintf("%s %s", r.Method, r.RequestURI)
	status := c.Status
	if status == 0 {
		status = 200
	}
	referer := r.Referer()
	if referer == "" {
		referer = "-"
	}
	userAgent := r.UserAgent()
	if userAgent == "" {
		userAgent = "-"
	}
	return fmt.Sprintf(`%s - %s [%s] "%s" %d %d "%s" "%s"`,
		r.RemoteAddr, user, timestamp, request, status, c.BodyLen,
		referer, userAgent)
}
