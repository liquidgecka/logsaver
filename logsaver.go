// COPYRIGHT

package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"

	"gitlab.com/liquidgecka/logsaver/banner"
	"gitlab.com/liquidgecka/logsaver/client"
	"gitlab.com/liquidgecka/logsaver/client/notify"
	"gitlab.com/liquidgecka/logsaver/log"
	"gitlab.com/liquidgecka/logsaver/server"
)

// A mapping of notify objects that were created via args.
var (
	notifyObjects = map[string]notify.Notify{}
	logger        = log.New()
)

// Gets the notification implementation for a given string. If the notifier
// does not exist then this will print an error and exit. If the notifier
// has already been initialized then this will return the singleton.
func getNotifier(name string) notify.Notify {
	if n, ok := notifyObjects[name]; ok {
		return n
	}
	n, err := notify.NewNotifyByType(name)
	if err != nil {
		fmt.Fprintf(os.Stderr,
			"Error getting notifier implementation '%s': %s\n",
			name, err)
		os.Exit(1)
	}
	go n.Run()
	notifyObjects[name] = n
	return n
}

// Default http client used for all HTTP calls.
var defaultClient *http.Client = &http.Client{}

// The parent context, and its cancel function.
var parentCtx context.Context
var parentCtxCancel func()

// Handles the 'server' command.
func argServer(args []string) ([]string, server.Server) {
	port := banner.Uint64{
		Min: 1,
		Max: 65535,
		Val: 2022,
		HelpString: "" +
			"The port to run the HTTP server on.",
	}
	destDir := banner.Path{
		Required:  true,
		MustExist: true,
		HelpString: "" +
			"The base directory to put received logs in.",
	}
	bannerArgs := map[string]banner.Flag{
		"port":     &port,
		"dest_dir": &destDir,
	}
	rArgs := banner.Parse("server:", bannerArgs, args)
	config := &server.Config{
		Logger:  logger,
		BaseDir: destDir.Val,
		Port:    int(port.Val),
	}
	svr, err := server.NewServer(config)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error configuring server: %s", err.Error())
		os.Exit(1)
	}
	return rArgs, svr
}

// Handles a 'dir_watch' command.
func argDirWatch(args []string) ([]string, client.DirWatcher) {
	backoff := banner.Duration{
		Min: 0,
		Max: time.Hour,
		Val: time.Millisecond * 50,
		HelpString: "" +
			"After a HTTP request fails the process will sleep a brief " +
			"period in\norder to prevent busy loops causing a thundering " +
			"heard of requests\nto overwhelm the Logsaver server.",
	}
	manualFileCheck := banner.Duration{
		Min: time.Second,
		Max: time.Hour * 24,
		Val: time.Hour,
		HelpString: "" +
			"If a file has not been modified in this period of time then " +
			"it will be\nchecked to see if it has been updated and " +
			"notification was lost.\nSetting this too low will cause " +
			"high CPU use, but setting it too high\nmight cause an update " +
			"to get missed.",
	}
	maximumBytesPerRequest := banner.Uint64{
		Min: 1024,
		Max: 1024 * 1024 * 1024 * 1024,
		Val: 64 * 1024 * 1024,
		HelpString: "" +
			"Each time a chunk of a file is uploaded to the remote " +
			"Logsaver server\nit will be limited to this many bytes.",
	}
	maximumRetries := banner.Uint64{
		Min: 1,
		Max: 1000 * 1000,
		Val: 5,
		HelpString: "" +
			"Each HTTP request will be repeated at most this many times.",
	}
	notify := banner.Enum{
		Options: notify.NotificationImplementations(),
		Val:     notify.DefaultNotificationName(),
		HelpString: "" +
			"The library to use when watching for file system updates.",
	}
	processingTimeout := banner.Duration{
		Min: time.Second,
		Max: time.Hour * 24 * 365,
		Val: time.Hour,
		HelpString: "" +
			"How long to keep trying a single HTTP request before marking " +
			"the whole file as having failed.",
	}
	requestTimeout := banner.Duration{
		Min: time.Second,
		Max: time.Hour * 24 * 365,
		Val: time.Minute * 5,
		HelpString: "" +
			"How long to give a single HTTP call before considering it " +
			"timed out. Note this must be enough time to finish uploading " +
			"the data to the remote server.",
	}
	baseURL := banner.URL{
		Required: true,
		HelpString: "" +
			"The URL and subpath of the remote Logsaver server.",
	}
	directory := banner.Path{
		Required:  true,
		MustExist: true,
		HelpString: "" +
			"The directory to scan for new files in.",
	}
	include := banner.Glob{
		Val: "*",
		HelpString: "" +
			"Upload any files that matches this glob pattern.",
	}
	exclude := banner.Glob{
		Val: "",
		HelpString: "" +
			"Exclude any file that does not match the include pattern above.",
	}
	fileType := banner.Enum{
		Options:  client.ValidFileTypes(),
		Required: true,
		HelpString: "" +
			"The type of file processor that should be used.",
	}
	bannerArgs := map[string]banner.Flag{
		"backoff":                   &backoff,
		"manual_file_check":         &manualFileCheck,
		"maximum_bytes_per_request": &maximumBytesPerRequest,
		"maximum_retries":           &maximumRetries,
		"notify":                    &notify,
		"processing_timeout":        &processingTimeout,
		"request_timeout":           &requestTimeout,
		"url":                       &baseURL,
		"directory":                 &directory,
		"include":                   &include,
		"exclude":                   &exclude,
		"type":                      &fileType,
	}
	rArgs := banner.Parse("dir_watch:", bannerArgs, args)

	// Setup the client configuration object.
	config := &client.DirWatcherConfig{
		Directory:               directory.Val,
		IncludeGlobs:            []string{include.Val},
		ExcludeGlobs:            []string{exclude.Val},
		URL:                     baseURL.Val.String(),
		HTTPClient:              defaultClient,
		Backoff:                 backoff.Val,
		MaximumBytesPerRequest:  int64(maximumBytesPerRequest.Val),
		MaximumRetries:          int(maximumRetries.Val),
		Notify:                  getNotifier(notify.Val),
		ProcessingTimeout:       processingTimeout.Val,
		RequestTimeout:          requestTimeout.Val,
		ManualFileCheckInterval: manualFileCheck.Val,
		ParentContext:           parentCtx,
		FileType:                fileType.Val,
	}
	d, err := client.NewDirWatcher(config)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error configuring watcher: %s", err.Error())
		os.Exit(1)
	}
	return rArgs, d
}

func main() {
	// Setup the context.
	parentCtx, parentCtxCancel = context.WithCancel(context.Background())

	logger.More()
	logger.More()
	logger.More()

	// Parse the default flags.
	flag.Parse()
	args := flag.Args()

	// A list of things that are being started.
	dirWatchers := []client.DirWatcher{}
	servers := []server.Server{}

	// Now we need to parse the remaining args in argument sets.
	for len(args) > 0 {
		switch args[0] {
		case "dir_watch":
			newArgs, dw := argDirWatch(args[1:])
			dirWatchers = append(dirWatchers, dw)
			args = newArgs
		case "server":
			newArgs, svr := argServer(args[1:])
			servers = append(servers, svr)
			args = newArgs
		default:
			fmt.Fprintf(os.Stderr, "Unknown command: %s\n", args[0])
			os.Exit(1)
		}
	}

	// Make sure that the server is supposed to be taking at least some
	// action. If nothing was configured we need to exit out.
	switch {
	case len(dirWatchers) > 0:
	case len(servers) > 0:
	default:
		fmt.Fprintf(os.Stderr, "No watchers were defined.\n")
		os.Exit(1)
	}

	logger.Infof("Starting the logsaver process.")

	// Setup a signal watcher that will shut us down on a SIGTERM.
	shutdownWG := sync.WaitGroup{}
	shutdownChan := make(chan os.Signal, 100)
	shutdownWG.Add(1)
	go func() {
		defer shutdownWG.Done()
		for range shutdownChan {
			for _, d := range dirWatchers {
				d.Shutdown()
			}
			for _, s := range servers {
				s.Stop()
			}
		}
	}()
	signal.Notify(shutdownChan, os.Interrupt)

	// Startup the workers for each of the configured things.
	wg := sync.WaitGroup{}

	// Start up each of the Directory Watchers
	for i, dw := range dirWatchers {
		wg.Add(1)
		go func(index int) {
			defer wg.Done()
			err := dw.Run()
			if err != nil {
				logger.Errorf(
					"Directory watcher %d failed: %s",
					index, err.Error())
			} else {
				logger.Infof("Directory watcher %d exited.", index)
			}
		}(i)
	}
	for i, s := range servers {
		wg.Add(1)
		go func(index int) {
			defer wg.Done()
			err := s.Run()
			if err != nil {
				logger.Errorf(
					"HTTP Server %d failed: %s",
					index, err.Error())
			} else {
				logger.Infof("HTTP Server %d exited.", index)
			}
		}(i)
	}

	// Wait for the running goroutines to finished.
	logger.Debugf("Waiting on processes to terminate.")
	wg.Wait()

	// Stop the shutdown routine.
	logger.Debugf("Waiting on the signal notifier to finalize.")
	signal.Reset(os.Interrupt)
	close(shutdownChan)
	shutdownWG.Wait()

	// And finally, exit.
	logger.Debugf("Terminating successfully.")
	os.Exit(0)
}
