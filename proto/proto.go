// COPYRIGHT

package proto

type HeadResponse struct {
	Size uint64 `json:"size,omitempty"`
}
