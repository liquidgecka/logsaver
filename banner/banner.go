// COPYRIGHT

// A flag replacement library that is a bit more powerful when dealing with
// default values and such.
package banner

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// Parses all of the flags provided in the given array of strings.
func Parse(title string, flags map[string]Flag, args []string) []string {
	set := flag.NewFlagSet("", flag.ExitOnError)
	for name, flag := range flags {
		set.Var(flag, name, flag.Help())
	}
	if err := set.Parse(args); err != nil {
		panic(err)
	}
	for name, flag := range flags {
		if flag.Missing() {
			if title != "" {
				fmt.Fprintf(os.Stderr, "%s\n", title)
			}
			fmt.Fprintf(os.Stderr, "  -%s is required (but missing).\n", name)
			fmt.Fprintf(os.Stderr, "\n")
			fmt.Fprintf(os.Stderr, "usage:\n")
			set.PrintDefaults()
			os.Exit(1)
		}
	}
	return set.Args()
}

type Flag interface {
	Get() interface{}
	Help() string
	Missing() bool
	Set(string) error
	String() string
}

// For processing Byte sizes (Special case of Uint64).
type Bytes struct {
	Min        uint64
	Max        uint64
	Val        uint64
	WasPassed  bool
	HelpString string
	Required   bool
}

func (b *Bytes) parseString(s string) (uint64, error) {
	// We expect the number to be in the form: <number><unit> where
	// number is an arbitrary unsigned integer, and unit is a known
	// SI or non-SI prefix. To handle this we first strip out all
	// spaces and commas from the string.
	newStrBuilder := &strings.Builder{}
	newStrBuilder.Grow(len(s))
	firstNonNumeric := len(s)
	for i, r := range s {
		if r == ',' || r == ' ' {
			continue
		}
		if (r >= '0' && r <= '9') || r == '.' {
			newStrBuilder.WriteRune(r)
		}
		if i < firstNonNumeric {
			firstNonNumeric = i
		}
	}
	newStr := newStrBuilder.String()

	// Next we identify the unit on the end of the number.
	mult := uint64(1)
	if firstNonNumeric < len(s) {
		unit := s[firstNonNumeric:len(s)]
		switch strings.ToLower(unit) {
		case "b":
			mult = 1
		case "kb":
			mult = 1000
		case "kib":
			mult = 1024
		case "mb":
			mult = 1000 * 1000
		case "mib":
			mult = 1024 * 1024
		case "gb":
			mult = 1000 * 1000 * 1000
		case "gib":
			mult = 1024 * 1024 * 1024
		case "tb":
			mult = 1000 * 1000 * 1000 * 1000
		case "tib":
			mult = 1024 * 1024 * 1024 * 1024
		default:
			return 0, fmt.Errorf("unknown unit: %s", unit)
		}
	}

	// Parse the integer component.
	if ui, err := strconv.ParseUint(newStr, 10, 64); err != nil {
		return 0, err
	} else {
		return mult * ui, nil
	}
}

func (b *Bytes) Help() string {
	return b.HelpString
}

func (b *Bytes) String() string {
	return "FIXME"
}

func (b *Bytes) Set(s string) error {
	if v, err := b.parseString(s); err != nil {
		return err
	} else if v < b.Min {
		return fmt.Errorf("must be more than %d", b.Min)
	} else if v > b.Max {
		return fmt.Errorf("must be less than %d", b.Max)
	} else {
		b.Val = v
		b.WasPassed = true
		return nil
	}
}

func (b *Bytes) Get() interface{} {
	return b.Val
}

func (b *Bytes) Missing() bool {
	return b.Required && !b.WasPassed
}

// Pick from list type.
type Enum struct {
	Options    []string
	IgnoreCase bool
	Val        string
	WasPassed  bool
	HelpString string
	Required   bool
}

func (e *Enum) Help() string {
	return e.HelpString
}

func (e *Enum) String() string {
	return "FIXME"
}

func (e *Enum) Set(s string) error {
	if e.IgnoreCase {
		s = strings.ToLower(s)
	}
	for _, option := range e.Options {
		if option == s {
			e.Val = s
			e.WasPassed = true
			return nil
		}
	}
	return fmt.Errorf("not one of [%s]", strings.Join(e.Options, ","))
}

func (e *Enum) Get() interface{} {
	return e.Val
}

func (e *Enum) Missing() bool {
	return e.Required && !e.WasPassed
}

// Durations for time.
type Duration struct {
	Min        time.Duration
	Max        time.Duration
	Val        time.Duration
	WasPassed  bool
	HelpString string
	Required   bool
}

func (d *Duration) Help() string {
	return d.HelpString
}

func (d *Duration) String() string {
	return "FIXME"
}

func (d *Duration) Set(s string) error {
	if v, err := time.ParseDuration(s); err != nil {
		return err
	} else if v < d.Min {
		return fmt.Errorf("must be more than %s", d.Min.String())
	} else if v > d.Max {
		return fmt.Errorf("must be less than %s", d.Max.String())
	} else {
		d.Val = v
		d.WasPassed = true
		return nil
	}
}

func (d *Duration) Get() interface{} {
	return d.Val
}

func (d *Duration) Missing() bool {
	return d.Required && !d.WasPassed
}

// Glob arguments
type Glob struct {
	Val        string
	WasPassed  bool
	HelpString string
	Required   bool
}

func (g *Glob) Help() string {
	return g.HelpString
}

func (g *Glob) String() string {
	return "FIXME"
}

func (g *Glob) Set(s string) error {
	if _, err := filepath.Match(s, ""); err != nil {
		return err
	}
	g.Val = s
	g.WasPassed = true
	return nil
}

func (g *Glob) Get() interface{} {
	return g.Val
}

func (g *Glob) Missing() bool {
	return g.Required && !g.WasPassed
}

// Integer types.
type Int64 struct {
	Min        int64
	Max        int64
	Val        int64
	WasPassed  bool
	HelpString string
	Required   bool
}

func (i *Int64) Help() string {
	return i.HelpString
}

func (i *Int64) String() string {
	return "FIXME"
}

func (i *Int64) Set(s string) error {
	if v, err := strconv.ParseInt(s, 10, 64); err != nil {
		return err
	} else if v < i.Min {
		return fmt.Errorf("must be more than %d", i.Min)
	} else if v > i.Max {
		return fmt.Errorf("must be less than %d", i.Max)
	} else {
		i.Val = v
		i.WasPassed = true
		return nil
	}
}

func (i *Int64) Get() interface{} {
	return i.Val
}

func (i *Int64) Missing() bool {
	return i.Required && !i.WasPassed
}

// Path types for files and directories.
type Path struct {
	Val        string
	MustExist  bool
	WasPassed  bool
	HelpString string
	Required   bool
}

func (p *Path) Help() string {
	return p.HelpString
}

func (p *Path) String() string {
	return "FIXME"
}

func (p *Path) Set(s string) error {
	if p.MustExist {
		if _, err := os.Stat(s); err != nil {
			return err
		}
	}
	p.Val = s
	p.WasPassed = true
	return nil
}

func (p *Path) Get() interface{} {
	return p.Val
}

func (p *Path) Missing() bool {
	return p.Required && !p.WasPassed
}

// String types
type String struct {
	AllowEmpty bool
	Val        string
	WasPassed  bool
	HelpString string
	Required   bool
}

func (s *String) Help() string {
	return s.HelpString
}

func (s *String) String() string {
	return "FIXME"
}

func (s *String) Set(ss string) error {
	if ss == "" && !s.AllowEmpty {
		return fmt.Errorf("can not be empty.")
	}
	s.Val = ss
	s.WasPassed = true
	return nil
}

func (s *String) Get() interface{} {
	return s.Val
}

func (s *String) Missing() bool {
	return s.Required && !s.WasPassed
}

// Integer types.
type Uint64 struct {
	Min        uint64
	Max        uint64
	Val        uint64
	WasPassed  bool
	HelpString string
	Required   bool
}

func (u *Uint64) Help() string {
	return u.HelpString
}

func (u *Uint64) String() string {
	return "FIXME"
}

func (u *Uint64) Set(s string) error {
	if v, err := strconv.ParseUint(s, 10, 64); err != nil {
		return err
	} else if v < u.Min {
		return fmt.Errorf("must be more than %d", u.Min)
	} else if v > u.Max {
		return fmt.Errorf("must be less than %d", u.Max)
	} else {
		u.Val = v
		u.WasPassed = true
		return nil
	}
}

func (u *Uint64) Get() interface{} {
	return u.Val
}

func (u *Uint64) Missing() bool {
	return u.Required && !u.WasPassed
}

// URLs
type URL struct {
	Val        url.URL
	WasPassed  bool
	HelpString string
	Required   bool
}

func (u *URL) Help() string {
	return u.HelpString
}

func (u *URL) String() string {
	return "FIXME"
}

func (u *URL) Set(s string) error {
	if p, err := url.Parse(s); err != nil {
		return err
	} else if p.Scheme == "" {
		return fmt.Errorf("missing a scheme (http://)")
	} else if p.Host == "" {
		return fmt.Errorf("missing a host")
	} else {
		u.Val = *p
		u.WasPassed = true
		return nil
	}
}

func (u *URL) Get() interface{} {
	return u.Val
}

func (u *URL) Missing() bool {
	return u.Required && !u.WasPassed
}
